/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.colsubsidio.entities;

import java.time.LocalDate;


public class Alumno {
   private String nombre;
   private String tel;
   private String dir;
   private int codigoest;
   private String fechaNac;

    public Alumno() {
    }

   
    public Alumno(String nombre, String tel, String dir, int codigoest, String fechaNac) {
        this.nombre = nombre;
        this.tel = tel;
        this.dir = dir;
        this.codigoest = codigoest;
        this.fechaNac = fechaNac;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }
   
   public int Edad(){
       LocalDate date = LocalDate.parse(this.fechaNac);
       LocalDate hoy = LocalDate.now();
       int ed = hoy.getYear()-date.getYear();
       
       return ed;
   }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public int getCodigoest() {
        return codigoest;
    }

    public void setCodigoest(int codigoest) {
        this.codigoest = codigoest;
    }
   
   
   
   
}

package com.colsubsidio.controller;

import com.colsubsidio.entities.Alumno;
import com.colsubsidio.services.AlumnoService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlumnoController {
    
    @Autowired
     private AlumnoService alumnoService;
    
    @RequestMapping("/alumnos")
    public ArrayList<Alumno> findAll(){
        return alumnoService.findAll();
    }
    
    @RequestMapping("/alumnos/{codigoest}")
    public Alumno getAlumno(@PathVariable int codigoest){
        return alumnoService.getAlumno(codigoest);
    }
    
    @PostMapping("/alumnos")
  public void save(@RequestBody Alumno alumno) {
    alumnoService.save(alumno);
  }
  
  @RequestMapping("/alumnos/edad/{codigoest}")
    public int getEdad(@PathVariable int codigoest){
        Alumno alumno= alumnoService.getAlumno(codigoest);
        return alumno.Edad();
    }
}

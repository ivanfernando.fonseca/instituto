/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.colsubsidio.services;

import com.colsubsidio.entities.Alumno;
import java.time.LocalDate;

import java.util.ArrayList;
import org.springframework.stereotype.Service;

/**
 *
 * @author DAGOBERTOANDRES
 */
@Service
public class AlumnoService {

    public AlumnoService() {
      this.alumnos.add(new Alumno("DAgo", "321", "dir", 1,"2018-10-30"));
      this.alumnos.add(new Alumno("Lorena", "321", "dir", 2, "2015-11-15"));
    }

    
    
    
    private ArrayList<Alumno> alumnos = new ArrayList<Alumno>();

    public ArrayList<Alumno> findAll() {
        return alumnos;
    }

    public int save(Alumno alumno) {
        alumnos.add(alumno);
        return alumno.Edad();
        
    }
    public Alumno getAlumno(int codigoest){
        for (int i = 0; i < alumnos.size(); i++) {
            if (alumnos.get(i).getCodigoest() == codigoest) {
               return  alumnos.get(i);
            }
        }
        return null;
    }
    
    public int  delete(int codigoest) {
        for (int i = 0; i < alumnos.size(); i++) {
            if (alumnos.get(i).getCodigoest() == codigoest) {
                alumnos.remove(i);
                return 1;
            }
        }
        return 0;
    }

}
